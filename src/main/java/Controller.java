import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class Controller {
    public Button button;
    public Label label;
    public Integer clickCounter;


    public void mouseClick(MouseEvent mouseEvent) {
        clickCounter=mouseEvent.getClickCount();
    }

    public void showMouseClick(ActionEvent actionEvent) {
        label.setText(this.clickCounter.toString());

    }
}
